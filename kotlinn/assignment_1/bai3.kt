package assignment_1

fun main() {
    println("Nhap so phan tu cua mang: ")
    var n = readLine()!!.toInt()
    var arr : IntArray = IntArray(n)

    println("\nNhap gia tri cac phan tu: ")
    for(i in 0 until n ){
        arr[i] = readLine()!!.toInt()
    }
    for (i in 0 until n) {
        for (j in i +1 until n)
            if(arr[i] > arr[j]) {
                var temp: Int = arr[i]
                arr[i] = arr[j]
                arr[j] = temp
            }
    }
    println("\nCac phan tu duoc sap xep theo thu tu tang dan: ")
    for (i in 0 until n) {
        print("${arr[i]} ")
    }
}
