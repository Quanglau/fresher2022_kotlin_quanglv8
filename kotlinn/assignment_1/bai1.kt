package assignment_1

fun main() {
    var q = 0;
    for (i in 10 until 200)
    {
        if(i % 7 == 0 && i % 5 != 0) {
            if(q != 0) {
                print(", ")
            }
            print("$i")
            q++;
        }
    }
}
