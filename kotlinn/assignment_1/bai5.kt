package assignment_1

fun main() {
    println("Nhap vao thang can xem: ")
    var thang = readLine()!!.toInt()
    println("Nhap nam can xem: ")
    var nam = readLine()!!.toInt()

    if (thang == 1 || thang == 3 || thang == 5 || thang == 7 || thang == 8 || thang == 10 || thang == 12) {
        print("thang co 31 ngay")
    } else if (thang == 4 || thang == 6 || thang == 9 || thang == 11) {
        print("thang co 30 ngay")
    } else if (thang == 2) {
        if (nam % 400 == 0) {
            print("thang co 29 ngay")
        } else if (nam % 4 == 0 && nam % 100 != 0) {
            print("thang co 29 ngay")
        } else {
            print("thang co 28 ngay")
        }
    }
}
