package assignment

import java.util.*

class nhanVien(){
    fun checkGioiTinh(str: String) : Int{
        if (str == "nam" || str == "nu") return 1
        return 0
    }
    fun checkNgay(str: String) : Int{
        if (str.length == 10) return 1
        return 0
    }
    fun checkTrinhDo(str: String) : Int {
        if (str.equals("Trung cap") || str.equals("Cao dang") ||str.equals("Dai hoc"))
            return 1
        else
            return 0
    }

     var name : String = ""
     var gioiTinh : String = ""
     var date : String = ""
     var number : String = ""
     var trinhDo : String = ""
     var maNV : String = ""

    fun update(){
        println("Nhap ten: ")
        name = readLine()!!.toString()
        while (name.length == 0){
            print("Nhap ten cua ban (khong de trong)")
            name = readLine()!!.toString()
        }


        println("Gioi tinh: (nam hoac nu) ")
        gioiTinh = readLine()!!.toString()
        while (checkGioiTinh(gioiTinh) == 0){
            println("Moi ban nhap lai gioi tinh")
            gioiTinh = readLine()!!.toString()
        }

        println("Nhap ngay thang nam sinh du 10 ky tu (dd/mm/yyyy)")
        date = readLine()!!.toString()
        while (checkNgay(date) == 0){
            println("Nhap lai ngay: ")
            date = readLine()!!.toString()
        }

        println("So dien thoai: ")
        number  = readLine()!!.toString()

        println("Trinh do chuyen mon: ('Trung cap','Cao dang', 'Dai hoc') ")
        trinhDo = readLine()!!.toString()
        while (checkTrinhDo(trinhDo) == 0){
            println("Nhap lai trinh do:('Trung cap','Cao dang', 'Dai hoc') ")
            trinhDo = readLine()!!.toString()
        }
    }

    fun add(){
        println("Ma nhan vien: ")
        maNV = readLine()!!.toString()
        while (maNV.length == 0){
            print("Ma nhan vien cua ban (khong de trong)")
            maNV = readLine()!!.toString()
        }
       update()

    }

    fun display() {
        println("Ma nha vien: $maNV")
        println("Ten: $name")
        println("Gioi tinh: $gioiTinh")
        println("Ngay sinh : $date")
        println("So dien thoai: $number")
        println("Trinh do chuyen mon: $trinhDo\n")
    }

}

fun timKiem(list: ArrayList<nhanVien>) {
    try {
        println("Tim kiem bang ten nhan vien chon 1")
        println("Tim kiem bang ma nhan vien chon 2")
        var n : String = readLine()!!.toString()
        when (n) {
            "1" -> {
                println("Nhap ten ")
                var ten: String = readLine()!!.toString()
                list.forEach {
                    if (it.name.equals(ten)) {
                        println(it.display())
                        return
                    }
                }
                println("Not found information")
            }
            "2" -> {
                println("Nhap ma nhan vien")
                var ma: String = readLine()!!.toString()
                list.forEach {
                    if (it.maNV == ma) {
                        println(it.display())
                        return
                    }
                }
                println("Not found information")

            }

        }
    }
    catch (e: InputMismatchException){
        timKiem(list)
    }
}

fun main(){

    println("\n_____ Quan ly nhan vien_____\n")
    println("1. add: thêm nhân viên ")
    println("2. display: Hiển thị danh sách nhân viên")
    println("3. update: Sua thong tin")
    println("4. find: Tim nhan vien")
    print("\nChon chuc nang so: ")
    var list  = ArrayList<nhanVien>()

    while (true) {
        var chon = readLine()
        when (chon) {

            "1" -> {
                var nv = nhanVien()
                nv.add()
                list.add(nv)
                print("\nChon chuc nang so: ")

            }
            "2" -> {
                if (list.isEmpty()) {
                    println("Not found information")
                }
                else
                    for (i in 0 until list.size) {
                        println(list.get(i).display())
                    }
                print("\nChon chuc nang so: ")
            }
            "3" -> {
                println("Nhap ma nhan vien ban muon chinh sua")
                var ma : String = readLine()!!.toString()
                list.forEach {
                    if (it.maNV == ma) {
                        it.update()

                    }
                }
                print("\nChon chuc nang so: ")

            }
            "4" -> {
                timKiem(list)
                print("\nChon chuc nang so: ")
            }

        }
    }
}

