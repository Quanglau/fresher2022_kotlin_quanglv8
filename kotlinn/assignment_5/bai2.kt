package assignment

fun nhap(s:IntArray, n : Int){
    var i : Int = 0
    try {
        println("Nhap cac gia tri cua mang: ")

        while (true){
            s[i] = readLine()!!.toInt()
            if (s[i] == 100){
                throw Exception("Ban da nhap so 100!!")
            }
            i++
        }
    }catch (e: Exception){
        e.printStackTrace()
    }finally {
        println("Mang sau khi nhap la: ")
        for (j in 0..i){
            print("${s[j]} ")
        }
    }
}

fun main(){
    println("Nhap so phan tu cua mang: ")
    var n : Int = readLine()!!.toInt()
    var s : IntArray = IntArray(n)
    nhap(s, n)
}