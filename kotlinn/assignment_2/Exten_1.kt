package assignment_2

import java.util.*

fun Int.toHexString():String{
    return this.toString(16).uppercase(Locale.getDefault())
}

fun String.toBinaryString() : String {
    return Integer.parseInt(this,16).toString(2)
}

fun main(){
    val hexStr = 200.toHexString()
    println("Chuyen so nguyen sang chuoi thap luc phan: $hexStr")
    val result = "C8".toBinaryString()
    println("Chuyen chuoi thap luc phan sang nhi phan: $result")

}