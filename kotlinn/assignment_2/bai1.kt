package assignment_2

fun so_nt(n : Int) :Int{
    if(n == 1 || n == 2) return 1
    for (i in 2..n-1){
        if(n % i == 0) return 0
    }
    return 1
}
fun main(){
    println("Nhap vao so can kiem tra: ")
    var n : Int = readLine()!!.toInt()
    println("Cac cap so nguyen to thoa man la: ")
    for (i in 1 until n/2){
        var b : Int = n-i
        if(so_nt(i) == 1 && so_nt(b) == 1 ){
            print("$n = $i + $b ")
            println()
        }
    }
}
