package assignment_2

fun so_ngt(n : Int) :Int{
    if(n == 1 || n == 2) return 1
    for (i in 2..n-1){
        if(n % i == 0) return 0
    }
    return 1
}
fun main(){
    println("Nhap so thu nhat: ")
    var a : Int = readLine()!!.toInt()
    println("Nhap so thu hai: ")
    var b : Int = readLine()!!.toInt()
    println("Cac so nguyen to o giua 2 so la: ")
    for (i in a..b){
        if (so_ngt(i) == 1) print("$i, ")
    }
}
