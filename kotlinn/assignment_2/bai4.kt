package assignment_2

fun main(){
    println("Nhap vao mot so: ")
    var n : Int = readLine()!!.toInt()
    var s : Int = 0
    while (n>0){
        s += n % 10
        n = n / 10
    }
    println("Tong cac chu so la: $s")
}
