package assignment_4

//bai 1
fun<T> count(arr: ArrayList<T>):Int {
    return arr.size
}

//bai 2
fun<T> swap(arrayList : ArrayList<T>, a: Int, b: Int) {

    val temp  = arrayList[a]
    arrayList[a] = arrayList[b]
    arrayList[b] = temp
}

//Bai 3
fun<T : Comparable<*>>max(arrayList: ArrayList<T>, begin: Int, end: Int) : T{
    var maxx : T = arrayList[0]
    for (i in begin..end) {
        if(compareValues(arrayList[i], maxx) > 0) {
            maxx = arrayList[i]
        }
    }
    return maxx
}

fun main() {

    var arr = ArrayList<Double>()
    arr.add(1.3)
    arr.add(3.5)
    arr.add(8.5)
    arr.add(5.0)
    arr.add(6.7)
    println("so phan tu ")
    println(com.example.kotlinbassic.count(arr))

    println("phan tu truoc khi doi cho ")
    for (i in arr){
        println(i)
    }

    swap(arr, 2, 3)
    println("phan tu sau doi cho ")
    for (i in arr){
        println(i)
    }
    var maxi = max(arr, 1, 4)
    println("Gia tri lon nhat: $maxi")

}

