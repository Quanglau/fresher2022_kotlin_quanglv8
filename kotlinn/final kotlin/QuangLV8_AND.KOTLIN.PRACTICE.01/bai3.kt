package com.example.afinal

import java.util.*

interface Queue<T> {
    fun getSize() : Int
    fun isEmpty(): Boolean
    fun peek(): T?
    fun enqueue(element: T)
    fun dequeue(): T?

}

class LinkedListQueue<T> : Queue<T> {

    var list = LinkedList<T>()

    override fun getSize(): Int {
        return list.size
    }

    override fun isEmpty(): Boolean {
        return list.isEmpty()
    }

    override fun peek(): T? {
        if (!isEmpty()) return list.first
        else return null
    }
    override fun enqueue(element: T){
        list.add(element)
    }

    override fun dequeue(): T? {
        if (!isEmpty()){
            val firstNode = list.first ?: return null
            list.removeFirst()
            return firstNode
        }
        else return null
    }

    override fun toString(): String = list.toString()

}

fun main(args: Array<String>) {
    var q1: Queue<Int> = LinkedListQueue()
    q1.enqueue(1)
    println("Queue is: " + q1)
    q1.dequeue()
    println("Queue is: " + q1)
    q1.dequeue()
    println("Queue is: " + q1)

    var q: Queue<Char> = LinkedListQueue()
    println("is Queue empty : "+q.isEmpty())
    q.enqueue('A')
    println("Queue is: " + q)
    println("is Queue empty : "+q.isEmpty())
    q.enqueue('B')
    println("Queue is: " + q)
    q.enqueue('C')
    println("Queue is: " + q)
    q.dequeue()
    println("Queue is: " + q)
    println("peek is : " +q.peek())
    println("peek is : " +q.peek())
    q.dequeue()
    println("Queue is: " + q)
    q.dequeue()
    println("Queue is: " + q)
    q.dequeue()
    println("Queue is: " + q)
}

