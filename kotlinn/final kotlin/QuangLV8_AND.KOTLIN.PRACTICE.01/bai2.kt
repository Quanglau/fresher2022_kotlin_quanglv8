package com.example.afinal
import java.util.*
import kotlin.collections.ArrayList
interface Stack<T> {
    fun getSize() : Int
    fun isEmpty(): Boolean
    fun peek(): T?
    fun put(element: T)
    fun pop(): T?
}

class linkedListStack<T> : Stack<T> {
    val elements: LinkedList<T> = LinkedList()


    override fun peek() : T? {
        return elements.lastOrNull()
    }
    override fun isEmpty() = elements.isEmpty()

    override fun getSize() = elements.size
    override fun toString(): String = elements.toString()
    override fun put(var1: T) {
        elements.add(var1)
    }
    override fun pop() : T? {
        val item = elements.lastOrNull()
        if (!isEmpty()){
            elements.removeAt(elements.size -1)
        }
        return item
    }

}



fun main(args: Array<String>) {
    var s: Stack<Int> = linkedListStack()
    s.put(1)
    println("Stack is: " + s)
    s.put(2)
    println("Stack is: " + s)
    s.pop()
    println("Stack is: " + s)
    s.pop()
    println("Stack is: " + s)
    s.pop()
    println("Stack is: " + s)
    println("is Stack empty : "+s.isEmpty())
    println("peek is : " +s.peek())
    println("the pop elements is : "+s.pop() )
    println("the size is : " +s.getSize())
    println("is Stack empty : " +s.isEmpty())
    println("Stack is: " + s)

}