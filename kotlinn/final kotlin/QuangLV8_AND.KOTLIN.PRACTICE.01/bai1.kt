package com.example.afinal

fun palindrome (inputt: String) : Boolean{

    var input = inputt.trim()
    val sb = StringBuilder(input)

    val reverseStr = sb.reverse().toString()

    return input.equals(reverseStr, ignoreCase = true)

}
fun main() {
    println(palindrome("abba"))
    println(palindrome("abcdefg"))
    println(palindrome("abba "))
    println(palindrome("abb a"))
}