package assignment_3

open class nhanVien (){
    var hoTen : String = ""
    var ngaySinh : String = ""

    open fun input(){
        println("Ho ten : ")
        hoTen = readLine()!!.toString()
        println("Ngay sinh: ")
        ngaySinh = readLine()!!.toString()
    }
    fun show(){
        println("Thong tin nhan vien")
        println("Ho ten : $hoTen")
        println("Ngay sinh: $ngaySinh")
    }


}
fun main(){
    var luong : Int = 0
    var luongCanBan : Int = 0
    var soSanPham : Int = 0
    var soNgayLam : Int = 0
    var nvsx = nhanVien()
    println("Nhap thong tin nhan vien san xuat")
    nvsx.input()
    println("Luong can ban: ")
    luongCanBan = readLine()!!.toInt()
    println("So san pham: ")
    soSanPham = readLine()!!.toInt()
    nvsx.show()
    luong = luongCanBan + (soSanPham * 5000)
    println("Luong: $luong\n\n")

    var nvvp = nhanVien()
    println("Nhap thong tin nhan vien van phong")
    nvvp.input()
    println("So ngay lam: ")
    soNgayLam = readLine()!!.toInt()
    nvvp.show()
    luong = soNgayLam * 100000
    println("Luong: $luong\n\n")

}
