package assignment_3

open class khachHang(){
    var soLuong : Int = 0
    var donGia : Int = 0

    init {
        this.soLuong = soLuong
        this.donGia = donGia
    }

    open fun inputKH(){
        println("So luong hang: ")
        soLuong = readLine()!!.toInt()
        println("Don gia hang: ")
        donGia = readLine()!!.toInt()
    }

    open fun output(){

    }
}

class loaiA() : khachHang(){
    var tienTra : Int = 0
    var ctyThu : Int = 0
    init {
        this.tienTra = tienTra
    }
    fun inputLoaiA(){
        super.inputKH()
    }

    override fun output() {
        super.output()
        tienTra = soLuong * donGia * 11/10
        println("So tien khach tra: $tienTra\n")
        ctyThu = donGia*soLuong
        println("So tien cong ty thu duoc la: $ctyThu")
    }
}

class loaiB() : khachHang(){
    var tienTra : Int = 0
    var phanTramKM : Int = 0
    var ctyThu : Int = 0

    init {
        this.tienTra = tienTra
        this.phanTramKM = phanTramKM
    }
    fun inputLoaiB(){
        super.inputKH()
        println("Phan tram khuyen mai: ")
        phanTramKM = readLine()!!.toInt()
    }

    override fun output() {
        super.output()
        tienTra = soLuong * donGia * (100 - phanTramKM + 10)/100
        println("So tien khach tra: $tienTra\n")
        ctyThu = donGia*soLuong * (100 -phanTramKM)/100
        println("So tien cong ty thu duoc la: $ctyThu")
    }
}
class loaiC() : khachHang(){
    var tienTra : Int = 0
    var ctyThu : Int = 0

    init {
        this.tienTra = tienTra
    }
    fun inputLoaiC(){
        super.inputKH()
    }

    override fun output() {
        super.output()
        tienTra = soLuong * donGia * 60/100
        println("So tien khach tra: $tienTra\n")
        ctyThu = donGia*soLuong * 50/100
        println("So tien cong ty thu duoc la: $ctyThu")
    }
}

fun main(){
    var a = loaiA()
    println("\nKhach hang binh thuong")
    a.inputLoaiA()
    a.output()

    var b = loaiB()
    println("\nKhach hang than thiet")
    b.inputLoaiB()
    b.output()

    var c = loaiC()
    println("\nKhach hang dac biet")
    c.inputLoaiC()
    c.output()
}

