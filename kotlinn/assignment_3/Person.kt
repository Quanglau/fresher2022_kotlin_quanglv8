package assignment_3

open class Person constructor(){
    private var name : String = ""
    private var gioitinh : String = ""
    private var ngaysinh : String = ""
    private var address : String = ""

    init {
        this.name = name
        this.gioitinh = gioitinh
        this.ngaysinh = ngaysinh
        this.address = address
    }
    open fun inputInfo(){
        print("Nhap ten: ")
        name = readLine()!!.toString()
        print("Nhap gioi tinh: ")
        gioitinh = readLine()!!.toString()
        print("Nhap ngay sinh: ")
        ngaysinh = readLine()!!.toString()
        print("Nhap dia chi: ")
        address = readLine()!!.toString()
    }
    open fun showInfo(){
        println("Ten: $name \n Gioi tinh: $gioitinh \n " +
                "Ngay sinh: $ngaysinh \n Address: $address")

    }
}


class Student ()
    : Person(){
    private var masv : String = ""
    private var diem : Double = 0.0
    private var email : String = ""
    init {
        this.masv = masv
        this.diem = diem
        this.email = email
    }
    override fun inputInfo(){
        println("\nNhap thong tin sinh vien")
        super.inputInfo()
        print("Nhap ma sinh vien: ")
        masv = readLine()!!.toString()
        print("Nhap diem (0.0 -> 10.0): ")
        diem = readLine()!!.toDouble()
        print("Nhap email (chua @ va khong co khoang trang): ")
        email = readLine()!!.toString()
    }
    override fun showInfo(){
        println("Thong tin sinh vien la: ")
        super.showInfo()
        print("Ma sinh vien: $masv \n Diem trung binh: $diem \n Email: $email")
    }
    fun hocBong() {
        if(diem >= 8.0) println("\nBan se nhan hoc bong")
        else println("\nBan chua du diem nhan hoc bong")
    }
}
class Teacher()
    : Person() {
    private var lopDay: String = ""
    private var luong: Double = 0.0
    private var soGio: Double = 0.0

    init {
        this.soGio = soGio
        this.lopDay = lopDay
        this.luong = luong
    }

    override fun inputInfo() {
        println("\nNhap thong tin giao vien")
        super.inputInfo()
        print("Nhap lop day (bat dau bang chu cai in hoa): ")
        lopDay = readLine()!!.toString()
        print("Nhap luong mot gio day: ")
        luong = readLine()!!.toDouble()
        print("Nhap so gio day trong thang: ")
        soGio = readLine()!!.toDouble()
    }

    override fun showInfo() {
        super.showInfo()
        println("Lop day la: $lopDay\n Luong mot gio: $luong\n So gio day trong thang: $soGio")
    }

    fun tinhLuong() {
        var luongNhan: Double
        if (lopDay == "L" || lopDay == "M"){
            luongNhan = luong * soGio + 500000
            println("Luong day lop buoi sang vs buoi chieu la: $luongNhan")
        }
        else{
            luongNhan = luong * soGio
            println("Luong day lop buoi toi la: $luongNhan")
        }
    }
}
fun main(){
    var sv  = Student()
    sv.inputInfo()
    sv.showInfo()
    sv.hocBong()

    var gv = Teacher()
    gv.inputInfo()
    gv.showInfo()
    gv.tinhLuong()
}
